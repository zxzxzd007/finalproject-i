﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    int countDownStart = 15;
    public Text timerUI;
    
     void Start()
    {
        countDownTimer();
    }
    
    void countDownTimer()
    {
        if (countDownStart > 0)
        {
            timerUI.text = "Timer :" + countDownStart;
            Debug.Log("Time : " + countDownStart);
            countDownStart--;
            Invoke("countDownTimer", 1.0f);
        }
        else
        {
            timerUI.text = "GameOver!";
            
        }
    }



    void Update()
    {
        
    }



   
    
}
