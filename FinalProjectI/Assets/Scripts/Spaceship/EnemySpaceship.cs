using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        [SerializeField] private AudioClip EnemyDeadSound;
        [SerializeField] private float EnemyDeadSoundVolume = 0.4f;
        public event Action OnExploded;

        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;
        private UnityEngine.Object explosionRef;
        //
        private int health = 5;
        private Material matWhite;
        private Material matDefaulth;
        SpriteRenderer sr;


        void Start()
        {
            explosionRef = Resources.Load("Explosion");
            //
            sr = GetComponent<SpriteRenderer>();
            matWhite = Resources.Load("WhiteFlash", typeof(Material)) as Material;
            matDefaulth = sr.material;
        }
        //
        private void OntriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Bullet"))
            {
                Destroy(collision.gameObject);
                health--;
                sr.material = matWhite;
                if (health <= 0)
                {
                    KillSelf();
                }
                else
                {
                    Invoke("ResetMaterial", .1f);
                }
            }
        }
        void ResetMaterial()
        {
            sr.material = matDefaulth;
        }

        private void KillSelf()
        {
            Destroy(gameObject);
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }


        public void TakeHit(int damage)
        {
            
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            

            Explode();
            AudioSource.PlayClipAtPoint(EnemyDeadSound, Camera.main.transform.position, EnemyDeadSoundVolume);
        }



        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
            GameObject explosion = (GameObject)Instantiate(explosionRef);
            explosion.transform.position = new Vector3(transform.position.x, transform.position.y + .3f, transform.position.z);
        }
       

        public override void Fire()
        {
            // TODO: Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
           
        }

        
    }
}