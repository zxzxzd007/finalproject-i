using Manager;
using System;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        [SerializeField] private AudioClip PlayerDeadSound;
        [SerializeField] private float playerDeadSoundVolume = 0.4f;
        public event Action OnExploded;


        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            audioSource = gameObject.GetComponent<AudioSource>();
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerFire);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
            AudioSource.PlayClipAtPoint(PlayerDeadSound, Camera.main.transform.position, playerDeadSoundVolume);
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}